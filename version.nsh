; Product and version information

; Full name
!define PROD_NAME "GoldenEye: Source"

; Name useable as a file path -- must not use any of \/:*?"<>|
; This is used as part of the file path for the downloaded patch file,
; so that the UAC dialog displays a recognizable and user-friendly file name.
!define FILE_NAME "GoldenEye Source"

; Game directory
!define MODDIR "gesource"

; Update executable file name
!define EXE_NAME "${MODDIR}_update.exe"

; Command line immediately following the file name
!define PATCH_EXE_FLAGS "/AutoUpdate"

; Minor version (e.g. 5.0, 5.1 etc)
; As a safegard, the updater will not work with versions other than this.
!define MINOR_VERSION "6.0"

; Default link to download page, unless overwritten by update.ini
!define DOWNLOAD_URL "https://www.geshl2.com/download/"

; Link to update file URL.
; Note this needs to have a corresponding signature "${UPDATE_FILE_URL}.sig"
!define UPDATE_FILE_URL "https://update.geshl2.com/installer/update_v${MINOR_VERSION}.txt"

; Useragent used by INETC plugin
!define INETC_USERAGENT "${MODDIR}-installer"

; Referer used by INETC plugin,
; the referer may be used to identify the installer for Swift container ACLs
!define INETC_REFERER "https://${MODDIR}-installer"

; Minisign public key
!define PUBLIC_KEY "RWQakOzqy89enIdl3eHy6pHW9ewqEwY8mWyOcF3PbDQxjpmg679Hn53t"
