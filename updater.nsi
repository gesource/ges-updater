; Multi-language support
Unicode true
; Run as the current user without elevating
RequestExecutionLevel user
; compression settings
SetCompressor zlib
SetDatablockOptimize ON

XPStyle on
ManifestDPIAware true

!addplugindir bin

!include version.nsh

Name "${PROD_NAME}"
OutFile "${EXE_NAME}"
Icon icon.ico

; Generate ${INSTALLER_BUILD_TIME} for file version, if not already defined
!ifndef INSTALLER_BUILD_TIME
  !tempfile __BUILTON
  !system 'powershell.exe -c "Write-Host -NoNewLine $\'!define INSTALLER_BUILD_TIME $\'; \
          (Get-Date).ToUniversalTime().ToString($\'yyyy.M.d.Hmm$\')" > "${__BUILTON}"'
  !include "${__BUILTON}"
  !delfile "${__BUILTON}"
!endif

; Set VIProductVersion -- prerequisite to VIAddVersionKey in language files
VIProductVersion "${INSTALLER_BUILD_TIME}"
; Load language files
!include nsh\lang_list.nsh
!include nsh\lang_load.nsh

; Function includes
!include FileFunc.nsh
!include WordFunc.nsh
!include nsh\GetMinorVersion.nsh
!include nsh\vdfParser.nsh

; Various variables
Var LOG_FILE_HANDLE ; Log file handle
Var TIME_LAUNCHED ; Timestamp of when the installer was opened. For naming log file.
Var INSTALLED_VERSION ; Currently installed version
; Override values
Var MODDIR ; Name of mod folder e.g. gesource
Var MINOR_VERSION ; Expected minor version series, e.g. 5.0 or 5.1, of the installed version
Var UPDATE_FILE_URL ; URL to update index
Var SIGNATURE_CHECK_ENABLE ; Default value 1; 0 disables signature check
; Update index values
Var PATCH_VERSION ; Available patch version
Var EXE_DOWNLOAD_URL ; Where to download the patch installer
Var UPDATER_VERSION_MIN ; Minimum updater version required by update index
Var DOWNLOAD_URL ; Link to open the website download page, as a fallback

!define PATCH_INDEX "$PLUGINSDIR\update.txt"
!define PATCH_EXE "$PLUGINSDIR\${FILE_NAME} $PATCH_VERSION Patch.exe"
!define UNINSTALL_REG_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\$MODDIR"

; Using the define instead of the run-time variable for MODDIR here is intentional
; as it won't be defined yet when the logger starts
!define LOG_FILE "$TEMP\${MODDIR}-updater-$TIME_LAUNCHED.log"

Function logger_start
	FileOpen $LOG_FILE_HANDLE "${LOG_FILE}" w
FunctionEnd

!macro logger text 
	FileWrite $LOG_FILE_HANDLE "${text}$\r$\n"
!macroend

; Macro for download showing a dialog
!macro download source destination outvar
	!insertmacro logger "Downloading ${source} to ${destination}"
	inetc::get /BANNER "$(INETC_BANNER)" /CAPTION "$(INETC_CAPTION)" /USERAGENT "${INETC_USERAGENT}" \
		/HEADER "Referer: ${INETC_REFERER}" "${source}" "${destination}" /END
	Pop ${outvar}
	!insertmacro logger "Downloader returned ${outvar}"
!macroend

; Macro for silent download; one that doesn't create a dialog
!macro download_silent source destination outvar
	!insertmacro logger "Downloading ${source} to ${destination}"
	inetc::get /SILENT /USERAGENT "${INETC_USERAGENT}" \
		/HEADER "Referer: ${INETC_REFERER}" "${source}" "${destination}" /END
	Pop ${outvar}
	!insertmacro logger "Downloader returned ${outvar}"
!macroend

!macro verify_signature verify_file
	!define SignatureCheck SigCheck_${__LINE__}
	!define SkipSignatureCheck SkipSigCheck_${__LINE__}
	StrCmp $SIGNATURE_CHECK_ENABLE 0 +1 ${SignatureCheck}
		!insertmacro logger "verify_signature: Signature check skipped because SIGNATURE_CHECK_ENABLE = 0"
		Goto ${SkipSignatureCheck}

	${SignatureCheck}:
	!insertmacro logger "Verifying signature ${verify_file}"
	Push $0 ; Store the current value of $0
	nsExec::Exec '"$PLUGINSDIR\minisign.exe" -Vm "${verify_file}" -P "${PUBLIC_KEY}"'
  	Pop $0 ; Get the Minisign return value off the stack

  	!insertmacro logger "Minisign returned $0"

  	StrCmp $0 0 +3 +1
  		!insertmacro logger "verify_signature: Setting error flag."
  		SetErrors

  	Pop $0 ; Set $0 back to its original value

  	${SkipSignatureCheck}:

  	!undef SignatureCheck
  	!undef SkipSignatureCheck
!macroend

Function ParseOverrides
	Push $0
	Push $1
	; Set default values based on defines in version.nsh
	; These may be overridden by this function
	StrCpy $MODDIR "${MODDIR}"
	StrCpy $MINOR_VERSION "${MINOR_VERSION}"
	StrCpy $UPDATE_FILE_URL "${UPDATE_FILE_URL}"
	StrCpy $SIGNATURE_CHECK_ENABLE 1

	; Overrides file will always be <updater file name>.txt (not including exe)
	; e.g. gesupdate.txt
	;
	; This file name is computed at runtime; we do that here.
	StrLen $0 $EXEPATH
	IntOp $0 $0 - 4
	StrCpy $0 $EXEPATH $0
	StrCpy $0 "$0.txt"

	IfFileExists $0 ParseFile
		!insertmacro logger "ParseOverrides: Could not find file $0"
		!insertmacro logger "ParseOverrides: No values overriden."
		Goto ParseCommandSwitches

	ParseFile:

	!insertmacro logger "ParseOverrides: Reading file $0"

	ClearErrors
	${ReadVDFStr} $1 $0 "updater_overrides>MODDIR"
	IfErrors MinorVersion +1
	StrCpy $MODDIR $1
	!insertmacro logger "ParseOverrides: MODDIR set to $MODDIR"

	MinorVersion:

	ClearErrors
	${ReadVDFStr} $1 $0 "updater_overrides>MINOR_VERSION"
	IfErrors UpdateFileUrl +1
	StrCpy $MINOR_VERSION $1
	!insertmacro logger "ParseOverrides: MINOR_VERSION set to $MINOR_VERSION"

	UpdateFileUrl:

	ClearErrors
	${ReadVDFStr} $1 $0 "updater_overrides>UPDATE_FILE_URL"
	IfErrors SignatureCheckEnable +1
	StrCpy $UPDATE_FILE_URL $1
	!insertmacro logger "ParseOverrides: UPDATE_FILE_URL set to $UPDATE_FILE_URL"

	SignatureCheckEnable:

	ClearErrors
	${ReadVDFStr} $1 $0 "updater_overrides>SIGNATURE_CHECK_ENABLE"
	IfErrors FinishParseFile +1
	StrCpy $SIGNATURE_CHECK_ENABLE $1
	!insertmacro logger "ParseOverrides: SIGNATURE_CHECK_ENABLE set to $SIGNATURE_CHECK_ENABLE"

	FinishParseFile:
	!insertmacro logger "ParseOverrides: Finished parsing $0"

	ParseCommandSwitches:
	!insertmacro logger "ParseOverrides: Parsing command-line options."

	${GetParameters} $0

	ClearErrors
	${GetOptions} $0 "/MODDIR=" $1
	IfErrors SMinorVersion +1
	StrCpy $MODDIR $1
	!insertmacro logger "ParseOverrides: MODDIR set to $MODDIR"

	SMinorVersion:

	ClearErrors
	${GetOptions} $0 "/MINOR_VERSION=" $1
	IfErrors SUpdateFileUrl +1
	StrCpy $MINOR_VERSION $1
	!insertmacro logger "ParseOverrides: MINOR_VERSION set to $MINOR_VERSION"

	SUpdateFileUrl:

	ClearErrors
	${GetOptions} $0 "/UPDATE_FILE_URL=" $1
	IfErrors SSignatureCheckEnable +1
	StrCpy $UPDATE_FILE_URL $1
	!insertmacro logger "ParseOverrides: UPDATE_FILE_URL set to $UPDATE_FILE_URL"

	SSignatureCheckEnable:

	ClearErrors
	${GetOptions} $0 "/SIGNATURE_CHECK_ENABLE=" $1
	IfErrors Return +1
	StrCpy $SIGNATURE_CHECK_ENABLE $1
	!insertmacro logger "ParseOverrides: SIGNATURE_CHECK_ENABLE set to $SIGNATURE_CHECK_ENABLE"

	Return:
	!insertmacro logger "ParseOverrides: Done."

	Pop $1
	Pop $0

FunctionEnd

Function ParseUpdateFile
	IfFileExists "${PATCH_INDEX}" ParseFile
		!insertmacro logger "ParseUpdateFile: Could not find file ${PATCH_INDEX}"
		MessageBox MB_OK|MB_ICONEXCLAMATION $(UPDATE_FAILED)
		Quit

	ParseFile:

	${ReadVDFStr} $PATCH_VERSION "${PATCH_INDEX}" "update>version"
	${ReadVDFStr} $DOWNLOAD_URL "${PATCH_INDEX}" "update>download_page"
	${ReadVDFStr} $EXE_DOWNLOAD_URL "${PATCH_INDEX}" "update>patch_installer"
	${ReadVDFStr} $UPDATER_VERSION_MIN "${PATCH_INDEX}" "update>updater_version"

	; If DOWNLOAD_URL is unset, set it to the default specified in version.nsh
	StrCmp $DOWNLOAD_URL "" +1 DownloadUrlSet
		!insertmacro logger "ParseUpdateFile: installer_download_page not set in update index. Setting to default value ${DOWNLOAD_URL}"
		StrCpy $DOWNLOAD_URL "${DOWNLOAD_URL}"

	DownloadUrlSet:

	; If UPDATER_VERSION_MIN is unset, set it to 0.0.0.0
	StrCmp $UPDATER_VERSION_MIN "" +1 UpdaterVersionSet
		StrCpy $UPDATER_VERSION_MIN "0.0.0.0"
		!insertmacro logger "ParseUpdateFile: updater_version not set in update index. Setting to default value 0.0.0.0"

	UpdaterVersionSet:

	!insertmacro logger "Finished parsing ${PATCH_INDEX}"
	!insertmacro logger " -- ParseUpdateFile values under $MINOR_VERSION -- "
	!insertmacro logger "version = $PATCH_VERSION"
	!insertmacro logger "installer_download_page = $DOWNLOAD_URL"
	!insertmacro logger "patch_installer = $EXE_DOWNLOAD_URL"
	!insertmacro logger "updater_version = $UPDATER_VERSION_MIN"
	!insertmacro logger " -- ParseUpdateFile end -- "

FunctionEnd

Function .onInit
	; Initialize plugins dir
	InitPluginsDir

	; Initialize $TIME_LAUNCHED variable
	${GetTime} "" "L" $0 $1 $2 $3 $4 $5 $6
	; $0="01"      day
	; $1="04"      month
	; $2="2005"    year
	; $4="16"      hour
	; $5="05"      minute
	; $6="50"      seconds
	StrCpy $TIME_LAUNCHED "$2$1$0$4$5$6"

	Call logger_start

	Call ParseOverrides

	; Extract Minisign
	SetOutPath $PLUGINSDIR
	File "bin\minisign.exe"

	; First, get the update index and signature
	!insertmacro download_silent "$UPDATE_FILE_URL" "${PATCH_INDEX}" $0
	!insertmacro download_silent "$UPDATE_FILE_URL.minisig" "${PATCH_INDEX}.minisig" $0

	ClearErrors
	!insertmacro verify_signature "${PATCH_INDEX}"
	IfErrors +1 PI_CorrectSignature
		MessageBox MB_OK|MB_ICONSTOP $(UPDATE_SIGNATURE_FAILED)
		Quit

	PI_CorrectSignature:

	Call ParseUpdateFile

	; Check updater version.
	; If updater is obsolete, display a message and offer to open the downloads page.
	${VersionCompare} ${INSTALLER_BUILD_TIME} $UPDATER_VERSION_MIN $0
	StrCmp $0 "2" +1 InstallerVersionPass
		!insertmacro logger "Minimum updater version $UPDATER_VERSION_MIN is greater than current version ${INSTALLER_BUILD_TIME}"
		MessageBox MB_YESNO|MB_ICONQUESTION $(UPDATER_OBSOLETE) IDNO IV_QUIT
			ExecShell open $DOWNLOAD_URL
		IV_QUIT:
			Quit

	InstallerVersionPass:

	; Get installed version from version.txt
	; Get sourcemods path, then look in sourcemods\$MODDIR\version.txt
	ReadRegStr $0 HKCU "SOFTWARE\Valve\Steam" "SourceModInstallPath"

	StrCpy $0 '$0\$MODDIR\version.txt'
	!insertmacro logger 'Getting installed version by checking $0'

	ClearErrors

	${ReadVDFStr} $INSTALLED_VERSION $0 'ges_version>text'

	IfErrors +1 VersionSet
		!insertmacro logger "Could not find an installed version (ReadVDFStr returned error)."
		MessageBox MB_YESNO|MB_ICONEXCLAMATION $(NOT_INSTALLED) IDNO NI_Quit
			ExecShell open $DOWNLOAD_URL
		NI_Quit:
			Quit		

	VersionSet:
	!insertmacro logger "Found installed version: $INSTALLED_VERSION"

	; If installed minor version number differs from our updater, error out
	Push $INSTALLED_VERSION
	Call GetMinorVersion
	Pop $0

	StrCmp $MINOR_VERSION $0 VersionsMatch
		!insertmacro logger "Detected installed version $INSTALLED_VERSION which differs from our updater's supported version(s) $MINOR_VERSION.x"
		MessageBox MB_OK|MB_ICONEXCLAMATION $(UPDATER_VERSION_MISMATCH)
		Quit

	VersionsMatch:

	; If patch version is unset or less than installed version,
	; show message that no update is available.
	StrCmp $PATCH_VERSION "" UpdateNotAvailable

	${VersionCompare} $PATCH_VERSION $INSTALLED_VERSION $0

	; If patch version is greater, continue. otherwise, show message
	StrCmp $0 1 PV_Continue

	UpdateNotAvailable:
		!insertmacro logger "Update not available. Installed version: $INSTALLED_VERSION. Available version: $PATCH_VERSION"
		MessageBox MB_OK $(UP_TO_DATE)
		Quit
	PV_Continue:

	; Download the patch installer, and show a popup while doing it
	; because this is the longest part. We don't show a popup window
	; at any other part until the installer launches.
	!insertmacro download $EXE_DOWNLOAD_URL "${PATCH_EXE}" $0
	StrCmp $0 "Cancelled" +1 DownloadCompleted
		MessageBox MB_OK|MB_ICONINFORMATION $(UPDATE_CANCELLED)
		!insertmacro logger $(UPDATE_CANCELLED)
		Quit

	DownloadCompleted:

	!insertmacro download_silent "$EXE_DOWNLOAD_URL.minisig" "${PATCH_EXE}.minisig" $0

	ClearErrors
	!insertmacro verify_signature "${PATCH_EXE}"
	IfErrors +1 EXE_CorrectSignature
		MessageBox MB_OK|MB_ICONSTOP $(UPDATE_EXE_SIGNATURE_FAILED)
		Quit

	EXE_CorrectSignature:

	!insertmacro logger 'Executing command: $\"${PATCH_EXE}$\" ${PATCH_EXE_FLAGS}'

	ExecShellWait open "${PATCH_EXE}" "${PATCH_EXE_FLAGS}"

	!insertmacro logger "Command finished; exiting."

	Quit
FunctionEnd

Section -
	; Dummy section
SectionEnd