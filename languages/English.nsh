; Initialize this language
; Full language list: http://pastebin.com/raw/JyV531w9
!insertmacro LANG_LOAD English

${LangString} UPDATE_FAILED "Failed to check for updates."
${LangString} UPDATER_OBSOLETE "An update is available which cannot be installed by this program. Would you like to open the Downloads page?"
${LangString} UPDATE_SIGNATURE_FAILED "$(UPDATE_FAILED)$\r$\n$\r$\nThe signature of the update index does not match."
${LangString} UPDATE_EXE_SIGNATURE_FAILED "$(UPDATE_FAILED)$\r$\n$\r$\nThe signature of the downloaded file does not match."
${LangString} UPDATER_VERSION_MISMATCH "Updater version does not match installed version. This program will now close."
${LangString} NOT_INSTALLED "Could not detect ${PROD_NAME} version. Would you like to open the download page?"
${LangString} UP_TO_DATE "No updates are available."
${LangString} UPDATE_CANCELLED "Update cancelled by user."
${LangString} INETC_BANNER "Preparing for update, please wait..."
${LangString} INETC_CAPTION "${PROD_NAME} Update"

; File version info
${VersionKey} "FileDescription" "${PROD_NAME} Update"

; The below keys should not be localized
${VersionKey} "ProductName" "${PROD_NAME}"
${VersionKey} "CompanyName" "Team GoldenEye: Source"
${VersionKey} "FileVersion" "${INSTALLER_BUILD_TIME}"
${VersionKey} "LegalCopyright" " "